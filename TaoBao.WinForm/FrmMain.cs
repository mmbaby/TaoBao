﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logic.Libraries.Static;
using Logic.Service;

namespace TaoBao.WinForm
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FrmMain : BaseForm
    {
        /// <summary>
        /// 窗体初始化
        /// </summary>
        public FrmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        OrderService orderService = new OrderService();

        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogin_Load(object sender, EventArgs e)
        {
            //设置浏览器的访问网址
            webBrowserForm.Navigate(TbUrl.IndelUrl);
            webBrowserForm.Dock = DockStyle.Fill;
            RefreshCookieContainer(webBrowserForm);
        }


        /// <summary>
        /// 刷新按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            //浏览器刷新
            webBrowserForm.Refresh();
            RefreshCookieContainer(webBrowserForm);
        }

        /// <summary>
        /// 全部订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnAllOrder_Click(object sender, EventArgs e)
        {
            RefreshCookieContainer(webBrowserForm);
            var res = await orderService.GetAllOrder(TbCookie);
        }

        /// <summary>
        /// 待付款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnObligation_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 待发货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSend_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 待收货
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReceived_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 待评价
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEvaluate_Click(object sender, EventArgs e)
        {

        }

        private void webBrowserForm_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            RefreshCookieContainer(webBrowserForm);
        }
    }
}
