﻿namespace TaoBao.WinForm
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbDDR = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnAllOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.btnObligation = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSend = new System.Windows.Forms.ToolStripMenuItem();
            this.btnReceived = new System.Windows.Forms.ToolStripMenuItem();
            this.btnEvaluate = new System.Windows.Forms.ToolStripMenuItem();
            this.webBrowserForm = new System.Windows.Forms.WebBrowser();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbRefresh,
            this.tsbDDR});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1120, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(52, 22);
            this.tsbRefresh.Text = "刷新";
            this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
            // 
            // tsbDDR
            // 
            this.tsbDDR.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAllOrder,
            this.btnObligation,
            this.btnSend,
            this.btnReceived,
            this.btnEvaluate});
            this.tsbDDR.Name = "tsbDDR";
            this.tsbDDR.Size = new System.Drawing.Size(69, 22);
            this.tsbDDR.Text = "我的订单";
            // 
            // btnAllOrder
            // 
            this.btnAllOrder.Name = "btnAllOrder";
            this.btnAllOrder.Size = new System.Drawing.Size(152, 22);
            this.btnAllOrder.Text = "全部订单";
            this.btnAllOrder.Click += new System.EventHandler(this.btnAllOrder_Click);
            // 
            // btnObligation
            // 
            this.btnObligation.Name = "btnObligation";
            this.btnObligation.Size = new System.Drawing.Size(152, 22);
            this.btnObligation.Text = "待付款";
            this.btnObligation.Click += new System.EventHandler(this.btnObligation_Click);
            // 
            // btnSend
            // 
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(152, 22);
            this.btnSend.Text = "待发货";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnReceived
            // 
            this.btnReceived.Name = "btnReceived";
            this.btnReceived.Size = new System.Drawing.Size(152, 22);
            this.btnReceived.Text = "待收货";
            this.btnReceived.Click += new System.EventHandler(this.btnReceived_Click);
            // 
            // btnEvaluate
            // 
            this.btnEvaluate.Name = "btnEvaluate";
            this.btnEvaluate.Size = new System.Drawing.Size(152, 22);
            this.btnEvaluate.Text = "待评价";
            this.btnEvaluate.Click += new System.EventHandler(this.btnEvaluate_Click);
            // 
            // webBrowserForm
            // 
            this.webBrowserForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserForm.Location = new System.Drawing.Point(0, 25);
            this.webBrowserForm.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserForm.Name = "webBrowserForm";
            this.webBrowserForm.ScriptErrorsSuppressed = true;
            this.webBrowserForm.ScrollBarsEnabled = false;
            this.webBrowserForm.Size = new System.Drawing.Size(1120, 654);
            this.webBrowserForm.TabIndex = 5;
            this.webBrowserForm.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowserForm_DocumentCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 657);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1120, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 679);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.webBrowserForm);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.Text = "登录";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.WebBrowser webBrowserForm;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton tsbDDR;
        private System.Windows.Forms.ToolStripMenuItem btnAllOrder;
        private System.Windows.Forms.ToolStripMenuItem btnObligation;
        private System.Windows.Forms.ToolStripMenuItem btnSend;
        private System.Windows.Forms.ToolStripMenuItem btnReceived;
        private System.Windows.Forms.ToolStripMenuItem btnEvaluate;
    }
}

