﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logic.Libraries.Static;
using Logic.Service;

namespace TaoBao.WinForm
{
    /// <summary>
    /// Form基类窗体
    /// </summary>
    public class BaseForm : Form
    {
        /// <summary>
        /// Cookie
        /// </summary>
        public static CookieContainer TbCookie = new CookieContainer();

        /// <summary>
        /// 刷新CookieContainer
        /// </summary>
        /// <param name="wb"></param>
        protected void RefreshCookieContainer(WebBrowser wb)
        {
            if (wb.Document == null) return;
            TbCookie = CookieService.GetCookieContainer(wb.Document.Cookie, "www.taobao.com");
        }
    }
}
