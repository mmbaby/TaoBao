﻿using System.Net;
using System.Threading.Tasks;
using Logic.Common;
using Logic.Libraries.Model;
using Logic.Libraries.Static;
using TaoBao.HttpHelper;
using TaoBao.HttpHelper.Enum;

namespace Logic.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderService
    {
        HttpHelper http = new HttpHelper();
        /// <summary>
        /// 获取所有订单信息
        /// </summary>
        /// <returns></returns>
        public Task<OrderArray> GetAllOrder(CookieContainer cc)
        {
            //1，进入淘宝主页，获取cookie
            HttpItem itemHome = new HttpItem()
            {
                URL = TbUrl.HomeUrl,
                Method = HttpMethod.Get,
                ContentType = "text/html; charset=utf-8",
                Host = "www.taobao.com",
                CookieContainer = cc,
                ResultType = ResultType.String,
                KeepAlive = true
            };
            //请求的返回值对象
            HttpResult resultHome = http.GetHtml(itemHome);

            //2，进入已买到的宝贝，获取cookie
            HttpItem item = new HttpItem()
            {
                URL = TbUrl.BuyertradeIndexUrl,
                Method = HttpMethod.Get,
                ContentType = "text/html;charset=GBK",
                Host = "buyertrade.taobao.com",
                CookieContainer = cc,
                ResultType = ResultType.String,
                KeepAlive = true
            };
            //请求的返回值对象
            HttpResult result = http.GetHtml(item);

            //3，获取所有订单
            string buyerNick = "";
            string dateBegin = "0";
            string dateEnd = "0";
            string lastStartRow = "";
            string logisticsService = "";
            string options = "0";
            string orderStatus = "0";
            string pageSize = "15";
            string queryBizType = "0";
            string queryOrder = "desc";
            string rateStatus = "";
            string refund = "";
            string sellerNick = "";
            string prePageNo = "2";
            string postData = $"buyerNick={buyerNick}&" +
                              $"dateBegin={dateBegin}&" +
                              $"dateEnd={dateEnd}&" +
                              $"lastStartRow={lastStartRow}&" +
                              $"logisticsService={logisticsService}&" +
                              $"options={options}&" +
                              $"orderStatus={orderStatus}&" +
                              $"pageSize={pageSize}&" +
                              $"queryBizType={queryBizType}&" +
                              $"queryOrder={queryOrder}&" +
                              $"rateStatus={rateStatus}&" +
                              $"refund={refund}&" +
                              $"sellerNick={sellerNick}&" +
                              $"prePageNo={prePageNo}";

            HttpItem itemBuyertrade = new HttpItem()
            {
                URL = TbUrl.BuyertradeUrl,
                Method = HttpMethod.Post,
                ContentType = "application/x-www-form-urlencoded; charset=UTF-8",
                Host = "buyertrade.taobao.com",
                CookieContainer = cc,
                ResultType = ResultType.String,
                KeepAlive = true,
                PostDataType = PostDataType.String,
                Postdata = postData
            };
            //请求的返回值对象
            HttpResult resultBuyertrade = http.GetHtml(itemBuyertrade);
            var orderArray = resultBuyertrade.Html.FromJson<OrderArray>();
            return Task.FromResult(orderArray);
        }
    }
}