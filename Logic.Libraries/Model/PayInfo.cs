﻿using System.Collections;
using System.Collections.Generic;

namespace Logic.Libraries.Model
{
    /// <summary>
    /// 支付信息
    /// </summary>
    public class PayInfo
    {
        /// <summary>
        /// 支付金额
        /// </summary>
        public string ActualFee { get; set; }

        public IList<IconsItem> Icons { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class IconsItem
    {
        public string LinkTitle { get; set; }
        public string LinkUrl { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
    /// <summary>
    /// 运费信息
    /// </summary>
    public class PostFreesItem
    {
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        /// <summary>
        /// 运费
        /// </summary>
        public string Value { get; set; }

    }
}