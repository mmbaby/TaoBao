﻿namespace Logic.Libraries.Model
{
    /// <summary>
    /// 交易信息
    /// 删除了订单详细和物流信息
    /// </summary>
    public class StatusInfo
    {
        /// <summary>
        /// 交易结果
        /// </summary>
        public string Text { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}