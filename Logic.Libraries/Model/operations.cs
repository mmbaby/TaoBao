﻿using System.Collections;
using System.Collections.Generic;

namespace Logic.Libraries.Model
{
    public class Operations
    {
        public IList<OpeItem> Item { get; set; }
    }

    public class OpeItem
    {
        public string Style { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}