﻿namespace Logic.Libraries.Model
{
    /// <summary>
    /// 订单集合
    /// </summary>
    public class OrderArray
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        public string Error { get; set; }
        public Extra Extra { get; set; }
        public OrderMain MainOrders { get; set; }
        public Page Page { get; set; }
    }
}