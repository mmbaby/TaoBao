﻿namespace Logic.Libraries.Model
{
    /// <summary>
    /// 分页实体
    /// </summary>
    public class Page
    {
        /// <summary>
        /// 页数
        /// </summary>
        public string PageNum { get; set; }
        /// <summary>
        /// 一页的数量
        /// </summary>
        public string PageSize { get; set; }
        /// <summary>
        /// 上一页
        /// </summary>  
        public string PrePageNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PrefetchCount { get; set; }
        /// <summary>
        /// 当前页 
        /// </summary>
        public string CurrentPage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string QueryForTitle { get; set; }
        /// <summary>
        /// 总数量
        /// </summary>
        public string TotalNumber { get; set; }
        /// <summary>
        /// 总页数
        /// </summary>
        public string TotalPage { get; set; }
    }
}