﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Libraries.Model
{
    public class Extra
    {
        public string BizType { get; set; }
        public string Currency { get; set; }
        public string CurrencySymbol { get; set; }
        public string Finish { get; set; }
        public string Id { get; set; }
        public string IsShowSellerService { get; set; }
        public string NeedDisplay { get; set; }
        public string TradeStatus { get; set; }
        public string Visibility { get; set; }
    }
}
