﻿using System.Collections.Generic;

namespace Logic.Libraries.Model
{
    /// <summary>
    /// 订单
    /// </summary>
    public class SubOrders
    {
        public IList<SubOrdersItem> Item { get; set; }
    }

    public class SubOrdersItem
    {
        public string Id { get; set; }
        public string ItemUrl { get; set; }
        public string Pic { get; set; }
        public string SnapUrl { get; set; }
        public string Title { get; set; }
        public string XtCurrent { get; set; }
        public string Quantity { get; set; }
        public IList<SkuTextItem> SkuText { get; set; }
        public IList<PriceInfo> PriceInfo { get; set; }
    }

    public class SkuTextItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// 价格信息
    /// </summary>
    public class PriceInfo
    {
        /// <summary>
        /// 实际价格
        /// </summary>
        public string Original { get; set; }
        /// <summary>
        /// 花费价格
        /// </summary>
        public string RealTotal { get; set; }
    }
}