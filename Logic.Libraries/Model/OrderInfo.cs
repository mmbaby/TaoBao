﻿namespace Logic.Libraries.Model
{
    /// <summary>
    /// 订单信息
    /// </summary>
    public class OrderInfo
    {
        /// <summary>
        /// 是否为B2C
        /// </summary>
        public string B2C { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public string CreateDay { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 主键，订单号
        /// </summary>
        public string Id { get; set; }
    }
}