﻿using System.Collections.Generic;

namespace Logic.Libraries.Model
{
    public class OrderMain
    {
        public Extra Extra { get; set; }
        public string Id { get; set; }
        public Operations Operations { get; set; }
        public OrderInfo OrderInfo { get; set; }
        public PayInfo PayInfo { get; set; }
        public Seller Seller { get; set; }
        public StatusInfo StatusInfo { get; set; }
        public SubOrders SubOrders { get; set; }
    }
}