﻿namespace Logic.Libraries.Model
{
    /// <summary>
    /// 商家
    /// </summary>
    public class Seller
    {
        public string AlertStyle { get; set; }
        public string GuestUser { get; set; }
        public string Id { get; set; }
        /// <summary>
        /// 店铺昵称
        /// </summary>
        public string Nick { get; set; }
        public string OpeanSearch { get; set; }
        public string ShopImg { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        public string ShopName { get; set; }
        /// <summary>
        /// 店铺地址
        /// </summary>
        public string ShopUrl { get; set; }
        public string WangwangType { get; set; }
    }
}