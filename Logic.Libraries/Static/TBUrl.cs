﻿namespace Logic.Libraries.Static
{
    /// <summary>
    /// 淘宝操作网址
    /// </summary>
    public class TbUrl
    {
        /// <summary>
        /// 登录页网址
        /// </summary>
        public static string IndelUrl = "https://login.taobao.com";

        /// <summary>
        /// 淘宝主页
        /// </summary>
        public static string HomeUrl = "https://www.taobao.com/";

        /// <summary>
        /// 已买到的宝贝
        /// GET
        /// 无参
        /// </summary>            
        public static string BuyertradeIndexUrl = "https://buyertrade.taobao.com/trade/itemlist/list_bought_items.htm";

        /// <summary>
        /// 获取所有订单
        /// POST请求
        /// 请求实体：Page
        /// 返回实体：OrderArray
        /// </summary>
        public static string BuyertradeUrl ="https://buyertrade.taobao.com/trade/itemlist/asyncBought.htm?action=itemlist/BoughtQueryAction&event_submit_do_query=1&_input_charset=utf8";
    }
}