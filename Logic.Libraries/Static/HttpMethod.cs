﻿namespace Logic.Libraries.Static
{
    public class HttpMethod
    {
        /// <summary>
        /// GET请求
        /// </summary>
        public static string Get = "Get";
        /// <summary>
        /// POST请求
        /// </summary>
        public static string Post = "POST";
    }
}